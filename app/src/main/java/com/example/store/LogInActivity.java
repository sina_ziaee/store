package com.example.store;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LogInActivity extends AppCompatActivity {

    EditText et_username;
    EditText et_password;
    CheckBox checkBox;
    Button btn_sign_in;
    TextView tv_go_to_sign_up;
    ImageView img_google;
    ImageView img_facebook;
    ImageView img_twitter;

    SharedPreferences appPreferences;

    private FirebaseAuth mFirebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        appPreferences = getSharedPreferences("settings", MODE_PRIVATE);
        boolean state = appPreferences.getBoolean("nightMode", false);
        if (state){
            setTheme(R.style.DarkMode);
        }
        else{
            setTheme(R.style.AppTheme);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        mFirebaseAuth = FirebaseAuth.getInstance();
        cast();
        setListeners();
    }

    private void cast() {
        et_password = findViewById(R.id.et_password_in_login);
        et_username = findViewById(R.id.et_username_in_login);
        btn_sign_in = findViewById(R.id.btn_signin);
        tv_go_to_sign_up = findViewById(R.id.tv_goto_sign_up);

    }

    private void setListeners() {
        tv_go_to_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LogInActivity.this, LogOnActivity.class));
                finish();
            }
        });
        btn_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });

    }

    private void register() {
        final String username_email = et_username.getText().toString();
        String password = et_password.getText().toString();

        if (username_email.isEmpty()){
            et_username.setError("Email required");
            et_password.requestFocus();
        }

        if (password.isEmpty()){
            et_password.setError("Password required");
            et_password.requestFocus();
        }

        final ProgressDialog dialoger = new ProgressDialog(this);
        dialoger.setCancelable(false);
        dialoger.setTitle("Signing in");
        dialoger.setMessage("Trying to find your information");
        dialoger.show();

        mFirebaseAuth.signInWithEmailAndPassword(username_email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            appPreferences.edit().putString("email", username_email).apply();
                            DatabaseReference ref = FirebaseDatabase.getInstance().getReference("users");
                            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    for (DataSnapshot ds:dataSnapshot.getChildren()){
                                        User user = ds.getValue(User.class);
                                        if (user.getEmail().equals(username_email)){
                                            appPreferences.edit().putString("username", user.getUsername()).apply();
                                            appPreferences.edit().putString("fullname", user.getName()).apply();
                                            if (!user.getPhotoURL().equals("")){
                                                appPreferences.edit().putString("photo", user.getPhotoURL()).apply();
                                            }
                                            dialoger.dismiss();
                                            Toast.makeText(LogInActivity.this, "Signed in", Toast.LENGTH_SHORT).show();
                                            startActivity(new Intent(LogInActivity.this, ActivityProfile.class));
                                            finish();
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });

                        }
                        else{
                            dialoger.dismiss();
                            String exception = task.getException().toString();
                            if (exception.endsWith("formatted")){
                                Toast.makeText(LogInActivity.this, "Please enter a correct email", Toast.LENGTH_SHORT).show();
                            }
                            else if (exception.endsWith("password")){
                                Toast.makeText(LogInActivity.this, "Either Email address or password is incorrect" + task.getException(), Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(LogInActivity.this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });

    }

}
