package com.example.store;

public class Item_comment {

    String username;
    String comment;
    String rating;

    public Item_comment(String username, String comment, String rating) {
        this.username = username;
        this.comment = comment;
        this.rating = rating;
    }

    public String getRating() {
        return rating;
    }

    public Item_comment() {

    }

    public String getUsername() {
        return username;
    }

    public String getComment() {
        return comment;
    }
}
