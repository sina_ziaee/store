package com.example.store;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import androidx.core.view.GravityCompat;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;


import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import de.hdodenhof.circleimageview.CircleImageView;

public class ActivityMain extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, NavigationView.OnNavigationItemSelectedListener {

    static DB_Helper db_helper;

    CircleImageView circleImageView;

    TextView tv_username;
    TextView tv_email;

    LinearLayout ll_settings, ll_about, ll_profile, ll_downloads, ll_favorite, ll_news;

    public static final int RC_SIGN_IN = 1;
    public static final String ANONYMOUS = "anonymous";
    private String mUsername;

    //************************ Firebase objects *******************************//

    private FirebaseDatabase mFirebase_database;
    private DatabaseReference mFirebase_database_reference_users;
    private ChildEventListener mFirebase_child_event_listener;

    private FirebaseAuth mFirebase_auth;
    private FirebaseAuth.AuthStateListener mFirebase_auth_listener;

    //*************************************************************************//

    DrawerLayout drawerLayout;
    NavigationView navigationView;
    ImageView img_drawer_toggle_in_main_activity;
    EditText et_in_main_activity;
    ImageView img_search_in_main_activity;
    FrameLayout frameLayout_in_main_activity;
    BottomNavigationView bottomNavigationView;

    SharedPreferences appPreferences;

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    protected void onCreate(Bundle savedInstanceState) {
        appPreferences = getSharedPreferences("settings", MODE_PRIVATE);
        boolean state = appPreferences.getBoolean("nightMode", false);
        if (state){
            setTheme(R.style.DarkMode);
        }
        else{
            setTheme(R.style.AppTheme);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!isNetworkConnected()){
            Toast.makeText(this, "Please check your internet connectivity", Toast.LENGTH_SHORT).show();
        }


        db_helper = new DB_Helper(this);

        cast();
        set_listener();
        if(savedInstanceState == null){
            bottomNavigationView.setSelectedItemId(R.id.menu_bottom_navigation_apps);
        }

        mFirebase_auth = FirebaseAuth.getInstance();

        fill_header_in_drawer();

    }

    private void fill_header_in_drawer() {
        String username = appPreferences.getString("username","-1");
        String email = appPreferences.getString("email", "-1");
        String photo_ref = appPreferences.getString("photo", "-1");
        if (!photo_ref.equals("-1")){
            Glide.with(this).load(photo_ref).into(circleImageView);
        }
        if (!username.equals("-1")){
            tv_email.setText(email);
            tv_username.setText(username);
        }
    }

    private void settingFirebaseInitializations() {

        if (appPreferences.getString("username", "Not set").equals("Not set")){
            startActivity(new Intent(ActivityMain.this, LogOnActivity.class));
        }
        else{
            Toast.makeText(this, appPreferences.getString("username", "anonymous"), Toast.LENGTH_SHORT).show();
            startActivity(new Intent(ActivityMain.this, ActivityProfile.class));
        }
    }

    private void cast() {
        mUsername = ANONYMOUS;
        drawerLayout = findViewById(R.id.drawer_in_main_activity);
        et_in_main_activity = findViewById(R.id.et_in_main_activity);
        img_search_in_main_activity = findViewById(R.id.img_search_in_main_activity);
        img_drawer_toggle_in_main_activity = findViewById(R.id.img_drawer_toggle_main_activity);
        navigationView = findViewById(R.id.nav_view_in_main_activity);
        frameLayout_in_main_activity = findViewById(R.id.fragment_container_in_main_activity);
        bottomNavigationView = findViewById(R.id.bottom_navigation_in_main_activity);
        // navigation drawer items
        ll_about = findViewById(R.id.about_activity);
        ll_settings = findViewById(R.id.settings_activity);
        ll_profile = findViewById(R.id.profile_activity);
        ll_downloads = findViewById(R.id.download_activity);
        ll_favorite = findViewById(R.id.favorite_activity);
        ll_news = findViewById(R.id.news_activity);

        circleImageView = findViewById(R.id.img_profiler);
        tv_username = findViewById(R.id.usernamer);
        tv_email = findViewById(R.id.emailer);

    }

    private void set_listener() {
        et_in_main_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityMain.this, ActivitySearch.class);
                startActivity(intent);
            }
        });

        img_search_in_main_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityMain.this, ActivitySearch.class);
                startActivity(intent);
            }
        });

        img_drawer_toggle_in_main_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        navigationView.setNavigationItemSelectedListener(this);

        ll_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingFirebaseInitializations();
            }
        });

        ll_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityMain.this, SettingsActivity.class);
                startActivity(intent);
            }
        });

        ll_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityMain.this, ActivityAbout.class);
                startActivity(intent);
            }
        });

        ll_downloads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (appPreferences.getString("username", "Not set").equals("Not set")){
                    startActivity(new Intent(ActivityMain.this, LogOnActivity.class));
                }
                else{
                    startActivity(new Intent(ActivityMain.this, ActivityDownloads.class));
                }
            }
        });
        ll_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityMain.this, ActivityFavoriteList.class));
            }
        });
        ll_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityMain.this, ActivityNews.class));
            }
        });



    }

    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        Fragment fragment = null;
        switch (id){
            case R.id.menu_bottom_navigation_apps:
                fragment = AppsFragment.newInstance();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container_in_main_activity,fragment)
                        .commit();
                break;
            case R.id.menu_bottom_navigation_movies:
                fragment = MoviesFragment.newInstance();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container_in_main_activity, fragment)
                        .commit();
                break;
        }
        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        return true;
    }

}
