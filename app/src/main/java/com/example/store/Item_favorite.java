package com.example.store;

public class Item_favorite {

    String name;
    String category;
    String photo_ref;
    float rate;
    String duration_size;
    String type;
    String description;

    public String getDescription() {
        return description;
    }

    public Item_favorite(String name, String category,
                         String photo_ref, float rate,
                         String duration_size, String type,
                         String description) {
        this.name = name;
        this.category = category;
        this.photo_ref = photo_ref;
        this.rate = rate;
        this.duration_size = duration_size;
        this.type = type;
        this.description = description;
    }

    public Item_favorite() {

    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    public String getPhoto_ref() {
        return photo_ref;
    }

    public float getRate() {
        return rate;
    }

    public String getDuration_size() {
        return duration_size;
    }

    public String getType() {
        return type;
    }
}
