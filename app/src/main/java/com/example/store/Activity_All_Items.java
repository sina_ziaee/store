package com.example.store;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class Activity_All_Items extends AppCompatActivity {

    RecyclerView recyclerView;
    Adapter_All_Items adapter;

    ImageView img_back;
    ImageView img_search;
    EditText et_search;

    String key;
    String type;

    static ProgressBar progressBar;

    SharedPreferences appPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        appPreferences = getSharedPreferences("settings", MODE_PRIVATE);
        boolean state = appPreferences.getBoolean("nightMode", false);
        if (state){
            setTheme(R.style.DarkMode);
        }
        else{
            setTheme(R.style.AppTheme);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_items);

        Intent intent = getIntent();
        key = intent.getStringExtra("key");
        type = intent.getStringExtra("type");
        cast();
        setListeners();
        setUpRecyclerView();
    }

    private void cast() {
        recyclerView = findViewById(R.id.recycler_view_items);
        img_back = findViewById(R.id.img_back_in_search_activity);
        img_search = findViewById(R.id.img_drawer_toggle_search_activity);
        et_search = findViewById(R.id.et_in_search_activity);
        et_search.setVisibility(View.GONE);
        img_search.setImageResource(R.drawable.ic_search);
        progressBar = findViewById(R.id.progressbar_items);
    }

    private void setListeners() {
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Activity_All_Items.this, ActivitySearch.class));
                finish();
            }
        });
    }

    private void setUpRecyclerView() {

        Query query;

        if (type.equals("movie")){
            query = FirebaseDatabase.getInstance().getReference("items").child(type).
                    orderByChild("category").equalTo(key);
        }
        else{
            query = FirebaseDatabase.getInstance().getReference("items").child(type).
                    orderByChild("category").equalTo(key);
        }

        FirebaseRecyclerOptions<Item_App> options = new FirebaseRecyclerOptions.Builder<Item_App>()
                .setQuery(query, Item_App.class)
                .build();

        adapter = new Adapter_All_Items(options);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
