package com.example.store;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class AdapterPager extends PagerAdapter {

    Context mContext;
    ArrayList<Item_App> list;
    LayoutInflater inflater;

    public AdapterPager(Context mContext, ArrayList<Item_App> list) {
        this.mContext = mContext;
        this.list = list;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = inflater.inflate(R.layout.view_pager_item, container, false);

        final Item_App item = list.get(position);
        ImageView imageView = view.findViewById(R.id.img_view);
        final TextView tv_name = view.findViewById(R.id.tv_name_view);
        TextView tv_view = view.findViewById(R.id.tv_views_view);
        TextView tv_rate = view.findViewById(R.id.tv_rate_view);

        Glide.with(mContext).load(item.getPhoto_url()).into(imageView);
        tv_name.setText(item.getName());
        tv_rate.setText(item.getRate());
        tv_view.setText(item.getViews()+"");

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                String key = tv_name.getText().toString();
                if (item.getType() == 1){
                    intent = new Intent(mContext, ActivityEachApp.class);
                }
                else{
                    intent = new Intent(mContext, ActivityEachMovie.class);
                }
                intent.putExtra("key", key);
                mContext.startActivity(intent);
            }
        });

        container.addView(view);

        return view;
    }
}
