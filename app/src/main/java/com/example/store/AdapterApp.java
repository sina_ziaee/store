package com.example.store;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;


public class AdapterApp extends FirebaseRecyclerAdapter<Item_App, AdapterApp.Holder> {

    Context mContext;

    public AdapterApp(@NonNull FirebaseRecyclerOptions<Item_App> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull Holder holder, int position, @NonNull Item_App model) {
        holder.tv_name.setText(model.getName());
        holder.tv_rate.setText(String.valueOf(model.getRate()));
        Glide.with(mContext).load(model.getPhoto_url()).into(holder.image);
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_app,
                parent, false);
        return new Holder(view);
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView image;
        TextView tv_name;
        TextView tv_rate;


        public Holder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.img_item_app);
            tv_name = itemView.findViewById(R.id.tv_name_item_app);
            tv_rate = itemView.findViewById(R.id.tv_rate_item_app);
            mContext = itemView.getContext();

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, ActivityEachApp.class);
            int position = getAdapterPosition();
            DatabaseReference dbRef = getSnapshots().getSnapshot(position).getRef();
            String key = dbRef.getKey();
            intent.putExtra("key", key);
            mContext.startActivity(intent);
        }
    }

    @Override
    public void onDataChanged() {
        super.onDataChanged();
        AppsFragment.progressBar.setVisibility(View.GONE);
        AppsFragment.rele1.setVisibility(View.VISIBLE);
        AppsFragment.rele2.setVisibility(View.VISIBLE);
        AppsFragment.rele3.setVisibility(View.VISIBLE);
        AppsFragment.rele4.setVisibility(View.VISIBLE);
    }
}