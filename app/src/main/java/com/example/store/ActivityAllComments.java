package com.example.store;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class ActivityAllComments extends AppCompatActivity {

    String key;

    RecyclerView recyclerView;

    AdapterComment adapter;

    ImageView img_back;
    TextView tv_title;

//    static ProgressBar progressBar;
    SharedPreferences appPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        appPreferences = getSharedPreferences("settings", MODE_PRIVATE);
        boolean state = appPreferences.getBoolean("nightMode", false);
        if (state){
            setTheme(R.style.DarkMode);
        }
        else{
            setTheme(R.style.AppTheme);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_comments);

        Intent intent = getIntent();
        key = intent.getStringExtra("key");

        cast();
        setListeners();
        setUpRecyclerView();

    }

    private void cast() {
        recyclerView = findViewById(R.id.recycler_view_all_comments);
        tv_title = findViewById(R.id.tv_in_other_activities);
        tv_title.setText("Comments about " + key);
        img_back = findViewById(R.id.img_back_in_other_activities);
//        progressBar = findViewById(R.id.progress_all_comments);
    }

    private void setListeners() {
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setUpRecyclerView() {
        Query query = FirebaseDatabase.getInstance().getReference("comments").child(key);

        FirebaseRecyclerOptions<Item_comment> options = new FirebaseRecyclerOptions.Builder<Item_comment>()
                .setQuery(query, Item_comment.class)
                .build();

        adapter = new AdapterComment(options);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }



}
