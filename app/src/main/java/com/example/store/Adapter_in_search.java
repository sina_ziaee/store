package com.example.store;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;

public class Adapter_in_search extends FirebaseRecyclerAdapter<Item_App, Adapter_in_search.Holder> {

    Context mContext;

    public Adapter_in_search(@NonNull FirebaseRecyclerOptions<Item_App> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull Holder holder, int position, @NonNull Item_App model) {
        holder.tv_name.setText(model.getName());
        holder.tv_type.setText(model.getType()+"");
        Glide.with(mContext).load(model.getPhoto_url()).into(holder.image);
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new Holder(view);
    }

    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tv_name;
        ImageView image;
        ImageView imageView_delete;
        TextView tv_type;

        public Holder(@NonNull View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name_item);
            image = itemView.findViewById(R.id.img_image_item);
            imageView_delete = itemView.findViewById(R.id.img_delete_item);
            tv_type = itemView.findViewById(R.id.type);
            imageView_delete.setVisibility(View.GONE);
            mContext = itemView.getContext();
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION){
                DatabaseReference dbRef = getSnapshots().getSnapshot(position).getRef();
                String key = dbRef.getKey();

                Intent intent;

                if (Integer.parseInt(tv_type.getText().toString()) == 1){
                    intent = new Intent(mContext, ActivityEachApp.class);
                }
                else{
                    intent = new Intent(mContext, ActivityEachMovie.class);
                }

                intent.putExtra("key", key);
                mContext.startActivity(intent);
            }
        }
    }

}
