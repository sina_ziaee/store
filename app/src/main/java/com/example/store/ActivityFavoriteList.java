package com.example.store;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ActivityFavoriteList extends AppCompatActivity {

    DB_Helper db_helper;
    TextView tv_show, tv_title;
    ImageView img_back;


    RecyclerView recyclerView;
    AdapterFavorite adapter;
    ArrayList<Item_favorite> download_list;

    SharedPreferences appPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        appPreferences = getSharedPreferences("settings", MODE_PRIVATE);
        boolean state = appPreferences.getBoolean("nightMode", false);
        if (state){
            setTheme(R.style.DarkMode);
        }
        else{
            setTheme(R.style.AppTheme);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_list);
        cast();
        setUpRecyclerView();
        setListeners();
    }

    private void setListeners() {
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tv_title.setText("Favorite list");

    }

    private void setUpRecyclerView() {

        db_helper = ActivityMain.db_helper;
        try {
            download_list = db_helper.find_records_from_db(db_helper.getReadableDatabase());
            adapter = new AdapterFavorite(download_list);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(adapter);
        }catch (SQLiteException e){
            e.printStackTrace();
            tv_show.setVisibility(View.VISIBLE);
        }

    }

    private void cast() {
        recyclerView = findViewById(R.id.recycler_view_in_favorite_list);
        tv_show = findViewById(R.id.tv_show);
        img_back = findViewById(R.id.img_back_in_other_activities);
        tv_title = findViewById(R.id.tv_in_other_activities);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
