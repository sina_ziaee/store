package com.example.store;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class ActivityProfileUpdate extends AppCompatActivity {

    int RC_PHOTO = 1;

    CircleImageView profile_image;

    Uri selectedImageUri;

    String photoURL;

    Intent intent;

    Button btn_cancel, btn_update;
    EditText et_fullname, et_phone;

    boolean img_selected = false;

    StorageReference imgRef = FirebaseStorage.getInstance().getReference("users_photos");
    DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("users");

    String username;
    SharedPreferences appPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        appPreferences = getSharedPreferences("settings", MODE_PRIVATE);
        username = appPreferences.getString("username", "Anonymous");
        dbRef = dbRef.child(username);
        boolean state = appPreferences.getBoolean("nightMode", false);
        if (state){
            setTheme(R.style.DarkMode);
        }
        else{
            setTheme(R.style.AppTheme);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_update);

        intent = getIntent();

        cast();
        setListeners();

    }

    private void cast() {
        profile_image = findViewById(R.id.img_profile_in_update);
        et_fullname = findViewById(R.id.et_fullName_in_update);
        et_phone = findViewById(R.id.et_phone_in_update);
        btn_cancel = findViewById(R.id.btn_cancel);
        btn_update = findViewById(R.id.btn_update);

    }

    private void setListeners() {
        profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_selected = true;
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                startActivityForResult(intent, RC_PHOTO);
            }
        });

        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadInfo();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED, intent);
                finish();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_PHOTO) {
            if (resultCode == RESULT_OK) {
                selectedImageUri = data.getData();
                profile_image.setImageURI(selectedImageUri);
            } else {
                img_selected = false;
                Toast.makeText(this, "Error You didn't pick an image", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void uploadInfo() {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Updating profile");
        progressDialog.setMessage("Please wait");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        if (selectedImageUri != null){
            final StorageReference stRef = imgRef.child(username);
            UploadTask uploadTask = stRef.putFile(selectedImageUri);

            uploadTask.continueWithTask(new Continuation() {
                @Override
                public Object then(@NonNull Task task) throws Exception {

                    if (!task.isSuccessful()){
                        Log.v("error", task.getException() + "");
                        throw task.getException();
                    }
                    return stRef.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()){

                        Uri downloadUri = task.getResult();
                        photoURL = downloadUri.toString();

                        String fullname = et_fullname.getText().toString();
                        String phone = et_phone.getText().toString();

                        HashMap<String, Object> map = new HashMap<>();

                        if (fullname.length() > 0){
                            map.put("name", fullname);
                        }
                        if (phone.length() > 0){
                            map.put("phone", phone);
                        }
                        if (photoURL.length() > 0){
                            map.put("photoURL", photoURL);
                        }
                        if (fullname.length() ==0 && phone.length() == 0 && photoURL.length() == 0){
                            Toast.makeText(ActivityProfileUpdate.this, "Noting to update", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        dbRef.updateChildren(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()){
                                    Toast.makeText(ActivityProfileUpdate.this, "Information successfully updated", Toast.LENGTH_SHORT).show();
                                }
                                else{
                                    Toast.makeText(ActivityProfileUpdate.this, "Failed to update information", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                        progressDialog.dismiss();

                        if (fullname.length() > 0){
                            appPreferences.edit().putString("name", fullname).apply();
                        }
                        if (phone.length() > 0){
                            appPreferences.edit().putString("phone", phone).apply();
                        }
                        if (photoURL.length() > 0){
                            appPreferences.edit().putString("photo", photoURL).apply();
                        }
                        if (fullname.length() ==0 && phone.length() == 0 && photoURL.length() == 0){
                            Toast.makeText(ActivityProfileUpdate.this, "Noting to update", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        intent.putExtra("photo",photoURL);
                        setResult(RESULT_OK, intent);
                        finish();

                    }
                    else{
                        progressDialog.dismiss();
                        Toast.makeText(ActivityProfileUpdate.this, "Bad internet connection", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_CANCELED, intent);
        finish();
    }
}
