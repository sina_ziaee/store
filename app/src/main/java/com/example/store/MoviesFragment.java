package com.example.store;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MoviesFragment extends Fragment {

    static ProgressBar progressBar;
    static RelativeLayout rele1;
    static RelativeLayout rele2;
    static RelativeLayout rele3;
    static RelativeLayout rele4;
    static RelativeLayout rele5;


    AdapterPager adapterPager;
    ViewPager viewPager;

    ImageView img_comedy, img_drama, img_action, img_crime, img_horror;

    AdapterMovie adapter_action, adapter_drama, adapter_comedy, adapter_horror, adapter_crime;

    RecyclerView rv_action, rv_comedy, rv_drama, rv_horror, rv_crime;

    DatabaseReference dataRef = FirebaseDatabase.getInstance().getReference("items/movies");
    Query query_action = dataRef.orderByChild("category").equalTo("Action").limitToFirst(10);
    Query query_drama = dataRef.orderByChild("category").equalTo("Drama").limitToFirst(10);
    Query query_comedy = dataRef.orderByChild("category").equalTo("Comedy").limitToFirst(10);
    Query query_horror = dataRef.orderByChild("category").equalTo("Horror").limitToFirst(10);
    Query query_crime = dataRef.orderByChild("category").equalTo("Crime").limitToFirst(10);

    public static MoviesFragment newInstance() {

        MoviesFragment fragment = new MoviesFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).
                inflate(R.layout.fragment_movies, container,false);

        cast(view);

        setListeners();

        fill_recycler_views(view);

        load_pager();

        return view;
    }

    private void load_pager() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("views/movies");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    ArrayList<Item_App> list = new ArrayList<>();
                    for (DataSnapshot ds:dataSnapshot.getChildren()){
                        Item_App item = ds.getValue(Item_App.class);
                        list.add(item);
                    }

                    adapterPager = new AdapterPager(getContext(),list);
                    viewPager.setAdapter(adapterPager);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void cast(View view){
        progressBar = view.findViewById(R.id.progress_movies_fragment);

        rele1 = view.findViewById(R.id.relative_1);
        rele2 = view.findViewById(R.id.relative_2);
        rele3 = view.findViewById(R.id.relative_3);
        rele4 = view.findViewById(R.id.relative_4);
        rele5 = view.findViewById(R.id.relative_5);

        viewPager = view.findViewById(R.id.viewPager_in_fg_movies);

        img_action = view.findViewById(R.id.img_right_action_item_movie);
        img_drama = view.findViewById(R.id.img_right_drama_item_movie);
        img_comedy = view.findViewById(R.id.img_right_comedy_item_movie);
        img_horror = view.findViewById(R.id.img_right_horror_item_movie);
        img_crime = view.findViewById(R.id.img_right_crime_item_movie);
    }

    private void setListeners() {
        final String type = "movies";

        img_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String key = "Action";
                Intent intent = new Intent(getContext(), Activity_All_Items.class);
                intent.putExtra("key", key);
                intent.putExtra("type", type);
                startActivity(intent);
            }
        });

        img_drama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String key = "Drama";
                Intent intent = new Intent(getContext(), Activity_All_Items.class);
                intent.putExtra("key", key);
                intent.putExtra("type", type);
                startActivity(intent);
            }
        });

        img_comedy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String key = "Comedy";
                Intent intent = new Intent(getContext(), Activity_All_Items.class);
                intent.putExtra("key", key);
                intent.putExtra("type", type);
                startActivity(intent);
            }
        });

        img_crime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String key = "Crime";
                Intent intent = new Intent(getContext(), Activity_All_Items.class);
                intent.putExtra("key", key);
                intent.putExtra("type", type);
                startActivity(intent);
            }
        });

        img_horror.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String key = "Horror";
                Intent intent = new Intent(getContext(), Activity_All_Items.class);
                intent.putExtra("key", key);
                intent.putExtra("type", type);
                startActivity(intent);
            }
        });
    }

    private void fill_recycler_views(View view) {

        rv_action = view.findViewById(R.id.rv_action_in_fg_movies);
        rv_drama = view.findViewById(R.id.rv_drama_in_fg_movies);
        rv_comedy = view.findViewById(R.id.rv_comedy_in_fg_movies);
        rv_horror = view.findViewById(R.id.rv_horror_in_fg_movies);
        rv_crime = view.findViewById(R.id.rv_crime_in_fg_movies);

        FirebaseRecyclerOptions<Item_movie> options1 = new FirebaseRecyclerOptions.Builder<Item_movie>()
                .setQuery(query_action, Item_movie.class)
                .build();

        adapter_action = new AdapterMovie(options1);
        rv_action.setHasFixedSize(true);
        rv_action.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rv_action.setAdapter(adapter_action);

        //*********************************************************************//

        FirebaseRecyclerOptions<Item_movie> options2 = new FirebaseRecyclerOptions.Builder<Item_movie>()
                .setQuery(query_comedy, Item_movie.class)
                .build();

        adapter_comedy = new AdapterMovie(options2);
        rv_comedy.setHasFixedSize(true);
        rv_comedy.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rv_comedy.setAdapter(adapter_comedy);

        //*********************************************************************//

        FirebaseRecyclerOptions<Item_movie> options3 = new FirebaseRecyclerOptions.Builder<Item_movie>()
                .setQuery(query_drama, Item_movie.class)
                .build();

        adapter_drama = new AdapterMovie(options3);
        rv_drama.setHasFixedSize(true);
        rv_drama.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rv_drama.setAdapter(adapter_drama);

        //*********************************************************************//

        FirebaseRecyclerOptions<Item_movie> options4 = new FirebaseRecyclerOptions.Builder<Item_movie>()
                .setQuery(query_horror, Item_movie.class)
                .build();

        adapter_horror = new AdapterMovie(options4);
        rv_horror.setHasFixedSize(true);
        rv_horror.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rv_horror.setAdapter(adapter_horror);

        //*********************************************************************//

        FirebaseRecyclerOptions<Item_movie> options5 = new FirebaseRecyclerOptions.Builder<Item_movie>()
                .setQuery(query_crime, Item_movie.class)
                .build();

        adapter_crime = new AdapterMovie(options5);
        rv_crime.setHasFixedSize(true);
        rv_crime.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rv_crime.setAdapter(adapter_crime);

    }

    @Override
    public void onStart() {
        super.onStart();
        adapter_comedy.startListening();
        adapter_drama.startListening();
        adapter_action.startListening();
        adapter_horror.startListening();
        adapter_crime.startListening();
        Log.v("check0", "onStart");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        adapter_action.stopListening();
        adapter_comedy.stopListening();
        adapter_drama.stopListening();
        adapter_crime.stopListening();
        adapter_horror.stopListening();
        Log.v("check0", "onStop");
    }
}
