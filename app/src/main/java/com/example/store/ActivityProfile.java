package com.example.store;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.auth.AuthUI;

import de.hdodenhof.circleimageview.CircleImageView;

public class ActivityProfile extends AppCompatActivity {

    DB_Helper db_helper;

    ImageView img_back;
    TextView tv_title;
    SharedPreferences appPreferences;
    ImageView img_settings;
    TextView tv_name;
    TextView tv_email;
    TextView tv_username;
    Button btn_sign_out;
    CircleImageView img_profile;
    private int RC_IMG = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        appPreferences = getSharedPreferences("settings", MODE_PRIVATE);
        boolean state = appPreferences.getBoolean("nightMode", false);
        if (state){
            setTheme(R.style.DarkMode);
        }
        else{
            setTheme(R.style.AppTheme);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        cast();
        setListener();

    }

    private void cast() {
        img_profile = findViewById(R.id.img_profile_in_profile);
        img_back = findViewById(R.id.img_back_in_profile_activity);
        tv_title = findViewById(R.id.tv_title_in_profile_activity);
        img_settings = findViewById(R.id.img_settings);
        tv_name = findViewById(R.id.tv_username_in_profile);
        tv_email = findViewById(R.id.tv_email_in_profile);
        tv_username = findViewById(R.id.tv_username_in_profile);
        btn_sign_out = findViewById(R.id.btn_sign_out);

        tv_name.setText(appPreferences.getString("fullname", "Not set"));
        tv_email.setText(appPreferences.getString("email", "Not set"));
        tv_username.setText(appPreferences.getString("username", "Not set"));
    }

    private void setListener() {
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        img_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(ActivityProfile.this, ActivityProfileUpdate.class), RC_IMG);
            }
        });

        btn_sign_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AuthUI.getInstance().signOut(ActivityProfile.this);
                SharedPreferences.Editor editor = appPreferences.edit();
                editor.putString("username", "Not set");
                editor.putString("email", "Not set");
                editor.putString("fullname","Not set");
                editor.putString("photo","Not set");
                editor.apply();
                Toast.makeText(ActivityProfile.this, "signed out", Toast.LENGTH_SHORT).show();
                db_helper = ActivityMain.db_helper;
                db_helper.drop_table_with_sign_out(db_helper.getWritableDatabase());
                startActivity(new Intent(ActivityProfile.this, ActivityMain.class));
                finish();
            }
        });

        String photoRef = appPreferences.getString("photo", "-1");
        if (!photoRef.equals("-1")){
            if (!photoRef.equals("Not set")){
                Glide.with(this).load(photoRef).into(img_profile);
            }
            else{
                img_profile.setImageResource(R.drawable.ic_user);
            }

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_IMG){
            if (resultCode == RESULT_OK){
                Bundle bundle = data.getExtras();
//                Uri uri = Uri.parse(bundle.getString("photo"));
//                img_profile.setImageURI(uri);
                String photoRef = bundle.getString("photo");
                Glide.with(this).load(photoRef).into(img_profile);
            }
            else{
                Toast.makeText(this, "Didn't update anything", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
