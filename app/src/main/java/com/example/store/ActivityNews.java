package com.example.store;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class ActivityNews extends AppCompatActivity {

    RecyclerView recyclerView;
    AdapterMessage adapter;
    static ProgressBar progressBar;
    ImageView imgback;
    TextView tv_title;

    SharedPreferences appPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        appPreferences = getSharedPreferences("settings", MODE_PRIVATE);
        boolean state = appPreferences.getBoolean("nightMode", false);
        if (state){
            setTheme(R.style.DarkMode);
        }
        else{
            setTheme(R.style.AppTheme);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        recyclerView = findViewById(R.id.recycler_view_news);
        progressBar = findViewById(R.id.progressbar_news);
        imgback = findViewById(R.id.img_back_in_other_activities);
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tv_title = findViewById(R.id.tv_in_other_activities);
        tv_title.setText("News");

        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("news");

        Query query = dbRef;

        FirebaseRecyclerOptions<Item_message> options = new FirebaseRecyclerOptions.Builder<Item_message>()
                .setQuery(query, Item_message.class)
                .build();

        adapter = new AdapterMessage(options);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
