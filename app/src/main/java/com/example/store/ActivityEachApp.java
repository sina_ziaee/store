package com.example.store;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

public class ActivityEachApp extends AppCompatActivity {
//************* progress bar **************//
    long timeLeft = 30000;
    boolean isRunning = false;
    CountDownTimer countDownTimer;
    ProgressBar progressBar;
    TextView tv_progress;
    LinearLayout download_container;
//*****************************************//


    boolean isPressed;
    Item_App item_app;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    ImageView img_back;
    EditText et_search;
    TextView tv_name;
    TextView tv_rate;
    TextView tv_views;
    RecyclerView recyclerView_comments;
    TextView tv_size;
    TextView tv_description;
    TextView tv_all_comments;
    Button btn_add_comment;
    ImageView img_item;
    ImageView img_search;
    String key;
    Button btn_install;
    ImageView img_add_favorite;

    String photoURL_in_firebase;

    int ratersCounter = 0;
    float ratersValue = 0;

    DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("items/apps");
    AdapterComment adapter;

    SharedPreferences appPreferences;

    DB_Helper db_helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        appPreferences = getSharedPreferences("settings", MODE_PRIVATE);
        boolean state = appPreferences.getBoolean("nightMode", false);
        if (state){
            setTheme(R.style.DarkMode);
        }
        else{
            setTheme(R.style.AppTheme);
        }        preferences = getPreferences(MODE_PRIVATE);
        editor = preferences.edit();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_each_app);

        db_helper = ActivityMain.db_helper;

        Intent intent = getIntent();
        key = intent.getStringExtra("key");
        Log.v("tag", key);
        ref = ref.child(key);
        cast();
        fill_blanks();
        setListeners();
        setUpCommentsRecyclerView();

    }

    private void setUpCommentsRecyclerView() {

        Query query = FirebaseDatabase.getInstance().getReference("comments/" + key).limitToFirst(4);

        FirebaseRecyclerOptions<Item_comment> options = new FirebaseRecyclerOptions.Builder<Item_comment>()
                .setQuery(query, Item_comment.class)
                .build();

        adapter = new AdapterComment(options);
        recyclerView_comments.setNestedScrollingEnabled(true);
        recyclerView_comments.setHasFixedSize(false);
        recyclerView_comments.setLayoutManager(new LinearLayoutManager(this));
        recyclerView_comments.setAdapter(adapter);

    }

    private void fill_blanks() {
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Item_App item = dataSnapshot.getValue(Item_App.class);
                item_app = item;
                tv_name.setText(item.getName());
                tv_rate.setText(item.getRate());
                tv_description.setText(item.getDescription());
                tv_size.setText(item.getSize());
                photoURL_in_firebase = item.getPhoto_url();
                tv_views.setText(item.getViews() + "");
                Glide.with(ActivityEachApp.this).load(item.getPhoto_url()).into(img_item);
                favoriteStateCheck();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void cast() {
        img_back = findViewById(R.id.img_back_in_search_activity);
        et_search = findViewById(R.id.et_in_search_activity);
        img_search = findViewById(R.id.img_drawer_toggle_search_activity);
        img_search.setImageResource(R.drawable.ic_search);
        tv_name = findViewById(R.id.tv_name_app);
        tv_description = findViewById(R.id.tv_description_app);
        tv_rate = findViewById(R.id.tv_rate_app);
        tv_size = findViewById(R.id.tv_size_app);
        tv_views = findViewById(R.id.tv_views_app);
        recyclerView_comments = findViewById(R.id.rv_comments_app);
        btn_add_comment = findViewById(R.id.btn_add_comment_app);
        tv_all_comments = findViewById(R.id.tv_all_comments_app);
        img_item = findViewById(R.id.img_image_app);
        et_search.setFocusableInTouchMode(false);
        btn_install = findViewById(R.id.btn_install_app);
        img_add_favorite = findViewById(R.id.img_add_favorite);

        progressBar = findViewById(R.id.progressbar_app);
        tv_progress = findViewById(R.id.tv_progress_app);
        download_container = findViewById(R.id.install_container);

    }

    private void setListeners() {

        img_add_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPressed){
                    img_add_favorite.setColorFilter(ContextCompat.getColor(
                            ActivityEachApp.this, R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);
                    db_helper.deleteFromDB(db_helper.getWritableDatabase(), key);
                }
                else{
                    save_in_db();
                    img_add_favorite.setColorFilter(ContextCompat.getColor(
                            ActivityEachApp.this, R.color.red), android.graphics.PorterDuff.Mode.SRC_IN);
                }
            }
        });

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tv_all_comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityEachApp.this, ActivityAllComments.class);
                intent.putExtra("key", key);
                startActivity(intent);
            }
        });
        btn_add_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                authentication();

            }
        });
        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityEachApp.this, ActivitySearch.class));
                finish();
            }
        });
        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityEachApp.this, ActivitySearch.class));
                finish();
            }
        });

        btn_install.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startStop();
            }
        });

    }

    private void authentication() {
        if (appPreferences.getString("username", "Not set").equals("Not set")){
            startActivity(new Intent(ActivityEachApp.this, LogOnActivity.class));
        }
        else{
            Toast.makeText(this, appPreferences.getString("username", "anonymous"), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(ActivityEachApp.this, ActivityAddComment.class);
            intent.putExtra("key", key);
            startActivity(intent);
        }
    }

    public void check_views(){
        final DatabaseReference[] viewRef = {FirebaseDatabase.getInstance().getReference("items/apps/" + key + "/views")};
        final ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                int num = dataSnapshot.getValue(Integer.class);
                num++;
                viewRef[0].setValue(num);
                tv_views.setText(String.valueOf(num));

                DatabaseReference mostViewsRef = FirebaseDatabase.getInstance().getReference("views/apps");
                mostViewsRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()){
                            final ArrayList<DatabaseReference> refList = new ArrayList<>();
                            ArrayList<Item_App> items = new ArrayList<>();
                            ArrayList<String> names = new ArrayList<>();
                            for (DataSnapshot ds: dataSnapshot.getChildren()){
                                Item_App item = ds.getValue(Item_App.class);
                                refList.add(ds.getRef());
                                items.add(item);
                                names.add(item.getName());
                            }
                            int min = 10000;
                            int index = 0;
                            for (int i=0;i<items.size();i++){
                                Log.v("minimum","views   " + items.get(i).getViews());
                                Log.v("minimum", "min    " + min);
                                if (min > items.get(i).getViews()){
                                    index = i;
                                    min = items.get(i).getViews();
                                }
                            }

                            int views = Integer.parseInt(tv_views.getText().toString());
                            String name = tv_name.getText().toString();
                            if (views > min && !names.contains(name)){
                                String rate = tv_rate.getText().toString();
                                String size = tv_size.getText().toString();
                                String description  = tv_description.getText().toString();
                                String photoUrl = photoURL_in_firebase;
                                int view = Integer.parseInt(tv_views.getText().toString());

                                HashMap<String, Object> map = new HashMap<>();
                                map.put("name", name);
                                map.put("rate", rate);
                                map.put("description", description);
                                map.put("size", size);
                                map.put("photo_url", photoUrl);
                                map.put("views", view);

                                refList.get(index).updateChildren(map);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        viewRef[0].addListenerForSingleValueEvent(valueEventListener);
    }

    public void check_rate(){
        final DatabaseReference reference = FirebaseDatabase.getInstance().getReference()
                .child("comments/" + key);

        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds: dataSnapshot.getChildren()){
                    String key0 = ds.getKey();
                    DatabaseReference rateRef = reference.child(key0);
                    ValueEventListener eventListener = new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Float rate = Float.parseFloat(dataSnapshot.child("rating").getValue(String.class));
                            ratersCounter++;
                            ratersValue += rate;

                            if (ratersCounter != 0){
                                float value = ratersValue / ratersCounter;
                                tv_rate.setText(String.valueOf(value).substring(0,3));
                                DatabaseReference ref = FirebaseDatabase.getInstance().getReference("items/apps/" + key + "/rate");
                                ref.setValue(String.valueOf(value));
                            }
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    };
                    rateRef.addListenerForSingleValueEvent(eventListener);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        reference.addListenerForSingleValueEvent(valueEventListener);
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
        check_rate();
        check_views();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    private void startStop() {
        if (isRunning){
            download_container.setVisibility(View.GONE);
            stopRunning();
            isRunning = false;
            btn_install.setText("Install");
        }
        else {
            download_container.setVisibility(View.VISIBLE);
            startRunning();
            isRunning = true;
            btn_install.setText("Installing");
        }
    }

    private void startRunning() {
        countDownTimer = new CountDownTimer(timeLeft, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeLeft = millisUntilFinished;
                updateTimer();
            }

            @Override
            public void onFinish() {
                btn_install.setText("Installed");
                Toast.makeText(ActivityEachApp.this, "Finished", Toast.LENGTH_SHORT).show();
                btn_install.setClickable(false);
                download_container.setVisibility(View.GONE);

                add_to_downloadList_in_firebase(tv_name.getText().toString());
            }
        }.start();
    }

    private void save_in_db() {
        String name = tv_name.getText().toString();
        String description = tv_description.getText().toString();
        String category = "app";
        float rate = Float.parseFloat(tv_rate.getText().toString());
        String duration_size = tv_size.getText().toString();
        String photoref = photoURL_in_firebase;
        String type = "app";
        Item_favorite item = new Item_favorite(name, category, photoref, rate, duration_size, type,description);
        db_helper.add_row_to_downloads_list(db_helper.getWritableDatabase(), item);

    }

    private void updateTimer() {
        int progress_updater = (int) (100 - (timeLeft/1000)*100/30);
        progressBar.setProgress(progress_updater);
        tv_progress.setText(progress_updater + " %");
    }

    private void stopRunning() {
        countDownTimer.cancel();
        isRunning = false;
    }

    public void add_to_downloadList_in_firebase(String name){
        final String name_temp = name;
        DatabaseReference user_ref = FirebaseDatabase.getInstance().getReference("users");
        String username = appPreferences.getString("username","anonymous");
        if (!username.equals("anonymous")){
            user_ref = user_ref.child(username + "/downloads");
            final DatabaseReference finalUser_ref = user_ref;
            user_ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()){
                        ArrayList<String> downloads = new ArrayList<>();
                        for (DataSnapshot ds : dataSnapshot.getChildren()){
                            String download_name = ds.getValue().toString();
                            downloads.add(download_name);
                        }
                        if (!downloads.contains(name_temp)){
                            downloads.add(name_temp);
                        }

                        finalUser_ref.setValue(downloads);

                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            DatabaseReference dbref = FirebaseDatabase.getInstance().getReference("downloads/"
                    +username);
            dbref.child(key).setValue(item_app);
        }
    }

    private void favoriteStateCheck() {
        isPressed = db_helper.check_if_item_is_in_favoriteList(db_helper.getReadableDatabase(), tv_name.getText().toString());
        if (isPressed){
            img_add_favorite.setColorFilter(ContextCompat.getColor(this, R.color.red), android.graphics.PorterDuff.Mode.SRC_IN);
        }
        else{
            img_add_favorite.setColorFilter(ContextCompat.getColor(this, R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);
        }
    }

}
