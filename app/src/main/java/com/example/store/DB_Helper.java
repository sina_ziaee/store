package com.example.store;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class DB_Helper extends SQLiteOpenHelper {

    public static final String database_name = "Store_database";
    private static final String table_name = "favorite";
    private static final String column_id = "_id";
    private static final String column_name = "name";
    private static final String column_type = "type";
    private static final String column_rate = "rate";
    private static final String column_duration_size = "duration_size";
    private static final String column_photo_ref = "photo_ref";
    private static final String column_category = "category";
    private static final String column_description = "description";

    public DB_Helper(@Nullable Context context) {
        super(context, database_name, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String create_table = "CREATE TABLE IF NOT EXISTS " + table_name
                + "(" + column_id + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + column_name + " TEXT UNIQUE ,"
                + column_type + " TEXT ,"
                + column_rate + " FLOAT ,"
                + column_duration_size + " TEXT ,"
                + column_photo_ref + " TEXT NOT NULL ,"
                + column_category + " TEXT ,"
                + column_description + " TEXT);";
        db.execSQL(create_table);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public ArrayList<Item_favorite> find_records_from_db(SQLiteDatabase db){
        String select = "SELECT * FROM " + table_name + ";";

        Cursor cursor = db.rawQuery(select, null);

        ArrayList<Item_favorite> list = new ArrayList<>();

        if (cursor.moveToFirst()){
            String name;
            String type;
            float rate;
            String duration_size;
            String category;
            String description;
            String photo_ref;

            do {
                name = cursor.getString(cursor.getColumnIndex(column_name));
                type = cursor.getString(cursor.getColumnIndex(column_type));
                rate = Float.parseFloat(cursor.getString(cursor.getColumnIndex(column_rate)));
                duration_size = cursor.getString(cursor.getColumnIndex(column_duration_size));
                category = cursor.getString(cursor.getColumnIndex(column_category));
                description = cursor.getString(cursor.getColumnIndex(column_description));
                photo_ref = cursor.getString(cursor.getColumnIndex(column_photo_ref));
                Item_favorite item = new Item_favorite(name,category,
                        photo_ref,rate,duration_size,type, description);
                list.add(item);

            }while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    public void add_row_to_downloads_list(SQLiteDatabase db, Item_favorite item) {
        ContentValues content = new ContentValues();
        content.put(column_name, item.getName());
        content.put(column_category, item.getCategory());
        content.put(column_description, item.getDescription());
        content.put(column_duration_size, item.getDuration_size());
        content.put(column_rate, item.getRate());
        content.put(column_photo_ref, item.getPhoto_ref());
        content.put(column_type, item.getType());

        long id = db.insert(table_name, null, content);
    }

    public void drop_table_with_sign_out(SQLiteDatabase db){
        String drop_table = "DROP TABLE IF EXISTS " + table_name + ";";
        db.execSQL(drop_table);
        onCreate(db);
    }

    public void deleteFromDB(SQLiteDatabase db, String name) {
        String delete_item = "DELETE FROM " + table_name + " WHERE " + column_name + "='" + name + "';";
        db.execSQL(delete_item);
    }

    public boolean check_if_item_is_in_favoriteList(SQLiteDatabase db, String name){
        String find = "Select * from " + table_name + " where " + column_name + "='" + name + "';";

        Log.v("tag", name);

        Cursor cursor = db.rawQuery(find, null);
        if (cursor.moveToFirst()){
            cursor.close();
            return true;
        }
        else{
            cursor.close();
            return false;
        }
    }
}
