package com.example.store;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

public class SettingsActivity extends AppCompatActivity {

    SwitchCompat nightMode, notification;
    TextView tv_title;
    ImageView img_back;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPreferences = getSharedPreferences("settings", MODE_PRIVATE);
        editor = sharedPreferences.edit();
        Log.v("tagger", sharedPreferences.getBoolean("nightMode", false)+"");
        if (sharedPreferences.getBoolean("nightMode", false)){
            setTheme(R.style.DarkMode);
        }
        else{
            setTheme(R.style.AppTheme);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        cast();
        boolean isNight = sharedPreferences.getBoolean("nightMode", false);
        if (isNight){
            nightMode.setChecked(true);
        }
        else{
            nightMode.setChecked(false);
        }
        setListeners();
    }

    private void cast() {
        nightMode = findViewById(R.id.switch_night_mode);
        notification = findViewById(R.id.switch_notification);
        img_back = findViewById(R.id.img_back_in_other_activities);
        tv_title = findViewById(R.id.tv_in_other_activities);
        tv_title.setText("Settings");
    }

    private void setListeners() {
        nightMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                boolean isNight = sharedPreferences.getBoolean("nightMode", false);
                if (isNight){
                    editor.putBoolean("nightMode", false).apply();
                }
                else{
                    editor.putBoolean("nightMode", true).apply();
                }
                restart();
            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingsActivity.this, ActivityMain.class));
                finish();
            }
        });
    }

    private void restart() {
        startActivity(new Intent(SettingsActivity.this, SettingsActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(SettingsActivity.this, ActivityMain.class));
        finish();
        super.onBackPressed();
    }
}
