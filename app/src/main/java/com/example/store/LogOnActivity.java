package com.example.store;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class LogOnActivity extends AppCompatActivity {

    EditText et_username;
    EditText et_fullname;
    EditText et_password;
    EditText et_re_password;
    EditText et_email;
    Button btn_signup;
    Button btn_go_to_login;

    private ProgressBar progressBar;

    SharedPreferences appPreferences;

    private FirebaseAuth mFirebaseAuth;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        appPreferences = getSharedPreferences("settings", MODE_PRIVATE);
        boolean state = appPreferences.getBoolean("nightMode", false);
        if (state){
            setTheme(R.style.DarkMode);
        }
        else{
            setTheme(R.style.AppTheme);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_on);
        cast();

        setListeners();
        mFirebaseAuth = FirebaseAuth.getInstance();

    }

    private void cast() {
        btn_go_to_login = findViewById(R.id.btn_goto_signin);
        et_username = findViewById(R.id.et_username_in_logon);
        et_fullname = findViewById(R.id.et_fullname_in_logon);
        et_password = findViewById(R.id.et_password_in_logon);
        et_re_password = findViewById(R.id.et_repassword_in_logon);
        et_email = findViewById(R.id.et_email_in_logon);
        btn_signup = findViewById(R.id.btn_sign_up_in_log_on);

        progressBar = findViewById(R.id.progressBar_in_logon);
        progressBar.setVisibility(View.GONE);
    }

    private void setListeners() {
        btn_go_to_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LogOnActivity.this, LogInActivity.class));
                finish();
            }
        });
        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });

    }

    private void register() {
        final String username = et_username.getText().toString();
        final String fullName = et_fullname.getText().toString();
        final String password = et_password.getText().toString();
        String rePassword = et_re_password.getText().toString();
        final String email = et_email.getText().toString();

        boolean email_matches = email.matches("[a-zA-Z]+@{1}[a-z]+.com");

        if (fullName.isEmpty()){
            et_fullname.setError("Name required");
            et_fullname.requestFocus();
            return;
        }

        if (username.isEmpty()){
            et_username.setError("Username required");
            et_username.requestFocus();
            return;
        }

        if (email.isEmpty()){
            et_email.setError("Email required");
            et_email.requestFocus();
            return;
        }

        if (password.isEmpty()){
            et_password.setError("Password required");
            et_password.requestFocus();
            return;
        }

        if (rePassword.isEmpty()){
            et_re_password.setError("Type password again");
            et_re_password.requestFocus();
            return;
        }

        if (!password.equals(rePassword)){
            Toast.makeText(this, "Password doesn't match", Toast.LENGTH_SHORT).show();
            return;
        }

        final ProgressDialog dialoger = new ProgressDialog(this);
        dialoger.setCancelable(false);
        dialoger.setTitle("Signing up");
        dialoger.setMessage("Trying to save the your information");

        dialoger.show();
        mFirebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            // means that the user is saved on firebase and now we can store
                            // the additional data to firebase realtime database
                            ArrayList<String> list = new ArrayList<>();
                            list.add("");
                            User user = new User(fullName, email, password, username, list,"","");
                            mFirebaseDatabase = FirebaseDatabase.getInstance();
                            mDatabaseReference = mFirebaseDatabase.getReference("users");

                            mDatabaseReference.child(user.getUsername()).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    dialoger.dismiss();
                                    if (task.isSuccessful()){
                                        Toast.makeText(LogOnActivity.this, "Information saved", Toast.LENGTH_SHORT).show();
                                        appPreferences.edit().putString("username", username).apply();
                                        appPreferences.edit().putString("email", email).apply();
                                        appPreferences.edit().putString("fullname", fullName).apply();
                                        startActivity(new Intent(LogOnActivity.this, ActivityProfile.class));
                                        finish();
                                    }
                                    else{
                                        String exception = task.getException().toString();
                                        Toast.makeText(LogOnActivity.this, exception, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });

                        }
                        else{
                            dialoger.dismiss();
                            if (task.getException().toString().endsWith("another account.")){
                                startActivity(new Intent(LogOnActivity.this, LogInActivity.class));
                                Toast.makeText(LogOnActivity.this, "This email account is being used", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                            else{
                                Toast.makeText(LogOnActivity.this, task.getException().toString(), Toast.LENGTH_SHORT).show();
                            }

                        }
                    }
                });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mFirebaseAuth.getCurrentUser() != null){

        }
    }
}
