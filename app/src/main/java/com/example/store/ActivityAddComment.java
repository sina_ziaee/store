package com.example.store;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ActivityAddComment extends AppCompatActivity {

    String key;

    String username;

    SharedPreferences appPreferences;

    EditText et_comment;
    Button btn_submit;

    RatingBar ratingBar;

    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("comments/");

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        appPreferences = getSharedPreferences("settings", MODE_PRIVATE);
        boolean state = appPreferences.getBoolean("nightMode", false);
        if (state){
            setTheme(R.style.DarkMode);
        }
        else{
            setTheme(R.style.AppTheme);
        }        username = appPreferences.getString("username", "Anonymous");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_comment);

        Intent intent = getIntent();
        key = intent.getStringExtra("key");
        ref = ref.child(key);

        et_comment = findViewById(R.id.et_comment);
        btn_submit = findViewById(R.id.btn_submit);
        ratingBar = findViewById(R.id.rating_bar);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Item_comment item = new Item_comment(username, et_comment.getText().toString(), String.valueOf(ratingBar.getRating()));
                ref.child(username).setValue(item).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            Toast.makeText(ActivityAddComment.this, "Comment submitted", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(ActivityAddComment.this, "Comment didn't submit", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

    }

}
