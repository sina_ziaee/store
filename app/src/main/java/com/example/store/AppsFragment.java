package com.example.store;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class AppsFragment extends Fragment {
    final ArrayList<Item_App> list_view = new ArrayList();

    AdapterPager adapterPager;
    ViewPager viewPager;

    ImageView img_google, img_microsoft, img_social_media,img_adobe;

    static ProgressBar progressBar;
    static RelativeLayout rele1;
    static RelativeLayout rele2;
    static RelativeLayout rele3;
    static RelativeLayout rele4;

    AdapterApp adapter_google;
    AdapterApp adapter_adobe;
    AdapterApp adapter_microsoft;
    AdapterApp adapter_social_media;

    RecyclerView rv_google;
    RecyclerView rv_microsoft;
    RecyclerView rv_social_media;
    RecyclerView rv_adobe;

    DatabaseReference dataRef = FirebaseDatabase.getInstance().getReference("items/apps");
    Query query_adobe = dataRef.orderByChild("category").equalTo("Adobe").limitToFirst(10);
    Query query_google = dataRef.orderByChild("category").equalTo("Google").limitToFirst(10);
    Query query_microsoft = dataRef.orderByChild("category").equalTo("Microsoft").limitToFirst(10);
    Query query_social_media = dataRef.orderByChild("category").equalTo("Social media").limitToFirst(10);

    public static AppsFragment newInstance() {

        AppsFragment fragment = new AppsFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).
                inflate(R.layout.fragment_apps, container,false);

        cast(view);

        setListeners();

        fill_recycler_views(view);

        load_pager();

        return view;
    }

    private void load_pager() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("views/apps");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    ArrayList<Item_App> list = new ArrayList<>();
                    for (DataSnapshot ds:dataSnapshot.getChildren()){
                        Item_App item = ds.getValue(Item_App.class);
                        list.add(item);
                    }

                    adapterPager = new AdapterPager(getContext(),list);
                    viewPager.setAdapter(adapterPager);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void cast(View view) {
        viewPager = view.findViewById(R.id.viewPager_in_fg_apps);
        progressBar = view.findViewById(R.id.progress_apps_fragment);
        rele1 = view.findViewById(R.id.rele_0);
        rele2 = view.findViewById(R.id.rele_1);
        rele3 = view.findViewById(R.id.rele_2);
        rele4 = view.findViewById(R.id.rele_3);
        img_google = view.findViewById(R.id.img_right_google_item_app);
        img_microsoft = view.findViewById(R.id.img_right_microsoft_item_app);
        img_social_media = view.findViewById(R.id.img_right_social_media_item_app);
        img_adobe = view.findViewById(R.id.img_right_adobe_item_app);
    }

    private void setListeners() {
        final String type = "apps";
        img_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String key = "Google";
                Intent intent = new Intent(getContext(), Activity_All_Items.class);
                intent.putExtra("type", type);
                intent.putExtra("key", key);
                startActivity(intent);
            }
        });

        img_social_media.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String key = "Social media";
                Intent intent = new Intent(getContext(), Activity_All_Items.class);
                intent.putExtra("type", type);
                intent.putExtra("key", key);
                startActivity(intent);
            }
        });

        img_microsoft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String key = "Microsoft";
                Intent intent = new Intent(getContext(), Activity_All_Items.class);
                intent.putExtra("type", type);
                intent.putExtra("key", key);
                startActivity(intent);
            }
        });
        img_adobe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String key = "Adobe";
                Intent intent = new Intent(getContext(), Activity_All_Items.class);
                intent.putExtra("type", type);
                intent.putExtra("key", key);
                startActivity(intent);
            }
        });
    }

    private void fill_recycler_views(View view) {
        rv_google = view.findViewById(R.id.rv_google_in_fg_apps);
        rv_microsoft = view.findViewById(R.id.rv_microsoft_in_fg_apps);
        rv_social_media = view.findViewById(R.id.rv_social_media_in_fg_apps);
        rv_adobe = view.findViewById(R.id.rv_adobe_in_fg_apps);

        FirebaseRecyclerOptions<Item_App> options1 = new FirebaseRecyclerOptions.Builder<Item_App>()
                .setQuery(query_google, Item_App.class)
                .build();

        adapter_google = new AdapterApp(options1);
        rv_google.setHasFixedSize(true);
        rv_google.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rv_google.setAdapter(adapter_google);

        //*********************************************************************//

        FirebaseRecyclerOptions<Item_App> options2 = new FirebaseRecyclerOptions.Builder<Item_App>()
                .setQuery(query_microsoft, Item_App.class)
                .build();

        adapter_microsoft = new AdapterApp(options2);
        rv_microsoft.setHasFixedSize(true);
        rv_microsoft.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rv_microsoft.setAdapter(adapter_microsoft);

        //*********************************************************************//

        FirebaseRecyclerOptions<Item_App> options3 = new FirebaseRecyclerOptions.Builder<Item_App>()
                .setQuery(query_social_media, Item_App.class)
                .build();

        adapter_social_media = new AdapterApp(options3);
        rv_social_media.setHasFixedSize(true);
        rv_social_media.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.HORIZONTAL, false));
        rv_social_media.setAdapter(adapter_social_media);

        //********************************************************************//

        FirebaseRecyclerOptions<Item_App> options4 = new FirebaseRecyclerOptions.Builder<Item_App>()
                .setQuery(query_adobe, Item_App.class)
                .build();

        adapter_adobe = new AdapterApp(options4);
        rv_adobe.setHasFixedSize(true);
        rv_adobe.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.HORIZONTAL, false));
        rv_adobe.setAdapter(adapter_adobe);

    }

    @Override
    public void onStart() {
        super.onStart();
        adapter_google.startListening();
        adapter_microsoft.startListening();
        adapter_social_media.startListening();
        adapter_adobe.startListening();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        adapter_google.stopListening();
        adapter_microsoft.stopListening();
        adapter_social_media.stopListening();
        adapter_adobe.stopListening();
    }

}
