package com.example.store;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ActivityDownloads extends AppCompatActivity {

    RecyclerView rv_apps;

    TextView tv_title;
    ImageView img_back;

    Adapter_Downloads adapterApp;
    static ProgressBar progressBar;

    String username;
    SharedPreferences appPreferences;
    DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("downloads");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        appPreferences = getSharedPreferences("settings", MODE_PRIVATE);
        boolean state = appPreferences.getBoolean("nightMode", false);
        if (state){
            setTheme(R.style.DarkMode);
        }
        else{
            setTheme(R.style.AppTheme);
        }        username = appPreferences.getString("username", "not set");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloads);
        cast();
        setUpRecyclerViews();
        setListeners();
    }

    private void setListeners() {
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tv_title.setText("Download list");
    }

    private void cast() {
        img_back = findViewById(R.id.img_back_in_other_activities);
        tv_title = findViewById(R.id.tv_in_other_activities);
        tv_title.setText("Downloads");
        rv_apps = findViewById(R.id.apps);
        progressBar = findViewById(R.id.progress_downloads);
    }

    private void setUpRecyclerViews() {

        FirebaseRecyclerOptions<Item_App> options_app = new FirebaseRecyclerOptions.Builder<Item_App>()
                .setQuery(dbRef.child(username).limitToFirst(5), Item_App.class)
                .build();

        adapterApp = new Adapter_Downloads(options_app);
        rv_apps.setLayoutManager(new LinearLayoutManager(this));
        rv_apps.setAdapter(adapterApp);

    }

    @Override
    protected void onStop() {
        super.onStop();
        adapterApp.stopListening();
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapterApp.startListening();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
