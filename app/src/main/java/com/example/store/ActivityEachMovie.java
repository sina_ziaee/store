package com.example.store;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

public class ActivityEachMovie extends AppCompatActivity {

    DB_Helper db_helper;
    Item_movie item_movie;

    //************* progress bar **************//
    long timeLeft = 30000;
    boolean isRunning = false;
    CountDownTimer countDownTimer;
    ProgressBar progressBar;
    TextView tv_progress;
    LinearLayout download_container;
    //*****************************************//

    DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("items/movies");
    AdapterComment adapter;

    String key;

    boolean isPressed;

    ImageButton img_add_to_favorite;

    TextView tv_views;
    TextView tv_name;
    TextView tv_rate;
    TextView tv_director;
    TextView tv_actors;
    TextView tv_duration;
    TextView tv_country;
    TextView tv_plot;
    ImageView img_image;
    ImageView img_back;
    EditText et_search;
    RecyclerView recyclerView_comments;
    Button btn_download;
    Button btn_add_comment;
    TextView tv_all_comments;
    TextView tv_year;
    ImageView img_search;

    SharedPreferences appPreferences;

    String photoURL_in_firebase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        appPreferences = getSharedPreferences("settings", MODE_PRIVATE);
        boolean state = appPreferences.getBoolean("nightMode", false);
        if (state){
            setTheme(R.style.DarkMode);
        }
        else{
            setTheme(R.style.AppTheme);
        }        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_each_movie);

        db_helper = ActivityMain.db_helper;

        Intent intent = getIntent();
        key = intent.getStringExtra("key");
        ref = ref.child(key);
        cast();
        fill_blanks();
        setListeners();
        setUpCommentsRecyclerView();

    }

    private void fill_blanks() {
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Item_movie item = dataSnapshot.getValue(Item_movie.class);
                item_movie = item;
                tv_name.setText(item.getName());
                tv_rate.setText(item.getRate());
                tv_director.setText(item.getDirector());
                tv_actors.setText(item.getActors());
                tv_year.setText(item.getYear());
                tv_country.setText(item.getCountries());
                tv_plot.setText(item.getPlot());
                tv_duration.setText(item.getDuration());
                photoURL_in_firebase = item.getPhoto_url();
                Glide.with(ActivityEachMovie.this).load(item.getPhoto_url()).into(img_image);
                favoriteStateCheck();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setUpCommentsRecyclerView() {

        Query query = FirebaseDatabase.getInstance().getReference().child("comments/" + key).limitToFirst(5);

        FirebaseRecyclerOptions<Item_comment> options = new FirebaseRecyclerOptions.Builder<Item_comment>()
                .setQuery(query, Item_comment.class)
                .build();

        adapter = new AdapterComment(options);
        recyclerView_comments.setNestedScrollingEnabled(true);
        recyclerView_comments.setHasFixedSize(false);
        recyclerView_comments.setLayoutManager(new LinearLayoutManager(this));
        recyclerView_comments.setAdapter(adapter);

    }

    private void cast() {
        tv_actors = findViewById(R.id.tv_directors_movie);
        tv_all_comments = findViewById(R.id.tv_all_comments_movie);
        tv_country = findViewById(R.id.tv_country_movie);
        tv_duration = findViewById(R.id.tv_duration_movie);
        tv_name = findViewById(R.id.tv_name_movie);
        et_search = findViewById(R.id.et_in_search_activity);
        img_search = findViewById(R.id.img_drawer_toggle_search_activity);
        img_search.setImageResource(R.drawable.ic_search);
        et_search.setText("");
        tv_plot = findViewById(R.id.tv_plot_movie);
        tv_rate = findViewById(R.id.tv_rate_movie);
        btn_download = findViewById(R.id.btn_download);
        btn_add_comment = findViewById(R.id.btn_add_comment_movie);
        img_back = findViewById(R.id.img_back_in_search_activity);
        img_image = findViewById(R.id.img_image_movie);
        tv_director = findViewById(R.id.tv_directors_movie);
        recyclerView_comments = findViewById(R.id.rv_comments_movie);
        tv_year = findViewById(R.id.tv_year_movie);
        et_search.setFocusableInTouchMode(false);
        tv_views = findViewById(R.id.tv_views_movie);

        progressBar = findViewById(R.id.progressbar_app);
        tv_progress = findViewById(R.id.tv_progress_app);
        download_container = findViewById(R.id.install_container);

        img_add_to_favorite = findViewById(R.id.img_add_favorite_in_movie);

    }

    private void setListeners() {
        tv_all_comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityEachMovie.this, ActivityAllComments.class);
                intent.putExtra("key",key);
                startActivity(intent);
            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startStop();
            }
        });
        btn_add_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                authentication();
            }
        });
        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityEachMovie.this, ActivitySearch.class));
                finish();
            }
        });
        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityEachMovie.this, ActivitySearch.class));
                finish();
            }
        });
        img_add_to_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPressed){
                    img_add_to_favorite.setColorFilter(ContextCompat.getColor(
                            ActivityEachMovie.this, R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);
                    db_helper.deleteFromDB(db_helper.getWritableDatabase(), key);
                }
                else{
                    save_in_db();
                    img_add_to_favorite.setColorFilter(ContextCompat.getColor(
                            ActivityEachMovie.this, R.color.red), android.graphics.PorterDuff.Mode.SRC_IN);
                }

            }
        });
    }

    public void check_views(){
        final DatabaseReference[] viewRef = {FirebaseDatabase.getInstance().getReference("items/movies/" + key + "/views")};
        final ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                int num = dataSnapshot.getValue(Integer.class);
                num++;
                viewRef[0].setValue(num);
                tv_views.setText(String.valueOf(num));

                DatabaseReference mostViewsRef = FirebaseDatabase.getInstance().getReference("views/movies");
                mostViewsRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()){
                            final ArrayList<DatabaseReference> refList = new ArrayList<>();
                            ArrayList<Item_movie> items = new ArrayList<>();
                            ArrayList<String> names = new ArrayList<>();
                            for (DataSnapshot ds: dataSnapshot.getChildren()){
                                Item_movie item = ds.getValue(Item_movie.class);
                                refList.add(ds.getRef());
                                items.add(item);
                                names.add(item.getName());
                            }
                            int min = 1000;
                            int index = 0;
                            for (int i=0;i<items.size();i++){
                                if (min > items.get(i).getViews()){
                                    index = i;
                                    min = items.get(i).getViews();
                                }
                            }

                            int views = Integer.parseInt(tv_views.getText().toString());
                            String name = tv_name.getText().toString();
                            if (views > min && !names.contains(name)){
                                String rate = tv_rate.getText().toString();
                                String size = tv_duration.getText().toString();
                                String description  = tv_plot.getText().toString();
                                String photoUrl = photoURL_in_firebase;
                                int view = Integer.parseInt(tv_views.getText().toString());
                                String director = tv_director.getText().toString();

                                HashMap<String, Object> map = new HashMap<>();
                                map.put("name", name);
                                map.put("rate", rate);
                                map.put("plot", description);
                                map.put("duration", size);
                                map.put("photo_url", photoUrl);
                                map.put("views", view);
                                map.put("director", director);

                                refList.get(index).updateChildren(map);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        viewRef[0].addListenerForSingleValueEvent(valueEventListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
        check_views();
    }

    private void startStop() {
        if (isRunning){
            download_container.setVisibility(View.GONE);
            stopRunning();
            isRunning = false;
            btn_download.setText("Download");
        }
        else {
            download_container.setVisibility(View.VISIBLE);
            startRunning();
            isRunning = true;
            btn_download.setText("Downloading");
        }
    }

    private void startRunning() {
        countDownTimer = new CountDownTimer(timeLeft, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeLeft = millisUntilFinished;
                updateTimer();
            }

            @Override
            public void onFinish() {
                btn_download.setText("Downloaded");
                Toast.makeText(ActivityEachMovie.this, "Finished", Toast.LENGTH_SHORT).show();
                btn_download.setClickable(false);
                download_container.setVisibility(View.GONE);
                add_to_downloadList_in_firebase(tv_name.getText().toString());
            }
        }.start();
    }

    private void updateTimer() {
        int progress_updater = (int) (100 - (timeLeft/1000)*100/30);
        progressBar.setProgress(progress_updater);
        tv_progress.setText(progress_updater + " %");
    }

    private void stopRunning() {
        countDownTimer.cancel();
        isRunning = false;
    }

    private void save_in_db() {
        String name = tv_name.getText().toString();
        String description = tv_plot.getText().toString();
        String category = "app";
        float rate = Float.parseFloat(tv_rate.getText().toString());
        String duration_size = tv_duration.getText().toString();
        String photoref = photoURL_in_firebase;
        String type = "movie";
        Item_favorite item = new Item_favorite(name, category, photoref, rate, duration_size, type,description);
        db_helper.add_row_to_downloads_list(db_helper.getWritableDatabase(), item);
    }

    public void add_to_downloadList_in_firebase(String name){
        Log.v("tag", "clicked");
        final String name_temp = name;
        DatabaseReference user_ref = FirebaseDatabase.getInstance().getReference("users");
        final String username = appPreferences.getString("username","anonymous");
        user_ref = user_ref.child(username + "/downloads");
        final DatabaseReference finalUser_ref = user_ref;
        user_ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    ArrayList<String> downloads = new ArrayList<>();
                    for (DataSnapshot ds : dataSnapshot.getChildren()){
                        String download_name = ds.getValue().toString();
                        downloads.add(download_name);
                    }
                    if (!downloads.contains(name_temp)){
                        downloads.add(name_temp);
                        finalUser_ref.setValue(downloads);
                    }


                    DatabaseReference dbref = FirebaseDatabase.getInstance().getReference("downloads/"
                            + username);
                    dbref.child(key).setValue(item_movie);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void favoriteStateCheck() {
        isPressed = db_helper.check_if_item_is_in_favoriteList(db_helper.getReadableDatabase(), tv_name.getText().toString());
        if (isPressed){
            img_add_to_favorite.setColorFilter(ContextCompat.getColor(this, R.color.red), android.graphics.PorterDuff.Mode.SRC_IN);
        }
        else{
            img_add_to_favorite.setColorFilter(ContextCompat.getColor(this, R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);
        }
    }

    private void authentication() {
        if (appPreferences.getString("username", "Not set").equals("Not set")){
            startActivity(new Intent(ActivityEachMovie.this, LogOnActivity.class));
        }
        else{
            Toast.makeText(this, appPreferences.getString("username", "anonymous"), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(ActivityEachMovie.this, ActivityAddComment.class);
            intent.putExtra("key", key);
            startActivity(intent);
        }
    }

}
