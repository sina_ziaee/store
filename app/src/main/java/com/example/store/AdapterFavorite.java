package com.example.store;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class AdapterFavorite extends RecyclerView.Adapter<AdapterFavorite.Holder> {

    String type;
    Context mContext;
    ArrayList<Item_favorite> list;
    DB_Helper db_helper = ActivityMain.db_helper;

    public AdapterFavorite(ArrayList<Item_favorite> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_favorite_list
        , parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        Item_favorite item = list.get(position);
        type = item.getType();
        holder.tv_name.setText(item.getName());
        holder.tv_rate.setText(item.getRate() + "");
        holder.tv_category.setText(item.getCategory());
        holder.tv_duration_size.setText(item.getDuration_size());
        Glide.with(mContext).load(item.getPhoto_ref()).into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imageView;
        TextView tv_name;
        TextView tv_rate;
        TextView tv_duration_size;
        TextView tv_category;
        ImageView img_delete;

        public Holder(@NonNull View itemView) {
            super(itemView);
            this.imageView = itemView.findViewById(R.id.img_image_favorite);
            this.tv_name = itemView.findViewById(R.id.tv_name_favorite);
            this.tv_rate = itemView.findViewById(R.id.tv_rate_favorite);
            this.tv_duration_size = itemView.findViewById(R.id.tv_duration_size_favorite);
            this.tv_category = itemView.findViewById(R.id.tv_category_favorite);
            this.img_delete = itemView.findViewById(R.id.img_delete_in_favorite_list);
            mContext = itemView.getContext();

            img_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    deleteItem(position);
                }
            });
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            String name = tv_name.getText().toString();
            Intent intent;
            if (type.equals("movie")){
                intent = new Intent(mContext, ActivityEachMovie.class);
                intent.putExtra("key", name);
                mContext.startActivity(intent);
            }
            else if (type.equals("app")){
                intent = new Intent(mContext, ActivityEachApp.class);
                intent.putExtra("key", name);
                mContext.startActivity(intent);
            }
            else{
                Toast.makeText(mContext, "Type not found", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void deleteItem(int position){
        Item_favorite item = list.get(position);
        db_helper.deleteFromDB(db_helper.getWritableDatabase(), item.getName());
        list.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, list.size());
    }

}
