package com.example.store;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.content.ClipboardManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityAbout extends AppCompatActivity {

    SharedPreferences appPreferences;

    TextView tv_gitlab_repo;
    ImageView img_back;
    TextView tv_title;
    Button btn_communicate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        appPreferences = getSharedPreferences("settings", MODE_PRIVATE);
        boolean state = appPreferences.getBoolean("nightMode", false);
        if (state){
            setTheme(R.style.DarkMode);
        }
        else{
            setTheme(R.style.AppTheme);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        cast();

    }

    private void cast() {
        tv_gitlab_repo = findViewById(R.id.tv_gitlab_repo);
        img_back = findViewById(R.id.img_back_in_other_activities);
        tv_title = findViewById(R.id.tv_in_other_activities);
        btn_communicate = findViewById(R.id.btn_communicate);

        tv_gitlab_repo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager cm = (ClipboardManager)getApplicationContext().getSystemService(getApplicationContext().CLIPBOARD_SERVICE);
                cm.setText(tv_gitlab_repo.getText());
                Toast.makeText(getApplicationContext(), "Copied to clipboard", Toast.LENGTH_SHORT).show();
            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tv_title.setText("About");

        btn_communicate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingFirebaseInitializations();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void settingFirebaseInitializations() {

        if (appPreferences.getString("username", "Not set").equals("Not set")){
            startActivity(new Intent(ActivityAbout.this, LogOnActivity.class));
            Toast.makeText(this, "Please sign in first", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(this, appPreferences.getString("username", "anonymous"), Toast.LENGTH_SHORT).show();
            startActivity(new Intent(ActivityAbout.this, ActivityCommunication.class));
        }
    }

}
