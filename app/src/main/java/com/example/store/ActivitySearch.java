package com.example.store;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class ActivitySearch extends AppCompatActivity {

    EditText et_search;
    ImageView img_back;
    ImageView img_toggle;
    SharedPreferences appPreferences;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    RecyclerView recyclerView;

    Spinner spinner;
    ArrayAdapter<String> spinnerAdapter;
    SwitchCompat switchCompat;
    TextView tv_category;

    Adapter_in_search adapter;

    boolean isMovie;

    DatabaseReference item_ref = FirebaseDatabase.getInstance().getReference("items");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        appPreferences = getSharedPreferences("settings", MODE_PRIVATE);
        boolean state = appPreferences.getBoolean("nightMode", false);
        if (state){
            setTheme(R.style.DarkMode);
        }
        else{
            setTheme(R.style.AppTheme);
        }
        isMovie = false;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        cast();
        et_search.requestFocus();
        set_listeners();

    }

    private void cast() {
        recyclerView = findViewById(R.id.recycler_view_in_search_activity);
        et_search = findViewById(R.id.et_in_search_activity);
        img_back = findViewById(R.id.img_back_in_search_activity);
        img_toggle = findViewById(R.id.img_drawer_toggle_search_activity);
        drawerLayout = findViewById(R.id.drawer_in_search);
        navigationView = findViewById(R.id.navigation_in_search_activity);
//        spinner = findViewById(R.id.spinner_in_search);
        switchCompat = findViewById(R.id.switch_in_search);
//        tv_category = findViewById(R.id.tv_category);

        //******************** Initializing the spinner *********************//
//        spinnerAdapter = new ArrayAdapter<String>(getApplicationContext(),
//                android.R.layout.simple_list_item_1,
//                getResources().getStringArray(R.array.filters_search_apps));
//        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinner.setAdapter(spinnerAdapter);
        //******************************************************************//
    }

    private void set_listeners() {
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                search(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        img_toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.END);
            }
        });

        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (!isChecked){
                    spinnerAdapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.layout.simple_list_item_1,
                            getResources().getStringArray(R.array.filters_search_apps));
//                    tv_category.setText("Category");
                    isMovie = false;
                }
                else{
                    spinnerAdapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.layout.simple_list_item_1,
                            getResources().getStringArray(R.array.filters_search_movies));
                    isMovie = true;
//                    tv_category.setText("Genre");
                }
//                spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                spinner.setAdapter(spinnerAdapter);
            }
        });
    }

    public void search(String item_name){
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if (isMovie == true){
            Query query = item_ref.child("movies").orderByChild("name")
                    .startAt(item_name)
                    .endAt(item_name + "\uf8ff");

            FirebaseRecyclerOptions<Item_App> options = new FirebaseRecyclerOptions.Builder<Item_App>()
                    .setQuery(query,Item_App.class)
                    .build();
            adapter = new Adapter_in_search(options);
            adapter.startListening();
            recyclerView.setAdapter(adapter);
        }
        else{
            Query query = item_ref.child("apps").orderByChild("name")
                    .startAt(item_name)
                    .endAt(item_name + "\uf8ff");

            FirebaseRecyclerOptions<Item_App> options = new FirebaseRecyclerOptions.Builder<Item_App>()
                    .setQuery(query,Item_App.class)
                    .build();
            adapter = new Adapter_in_search(options);
            adapter.startListening();
            recyclerView.setAdapter(adapter);
            Log.v("tag",item_name);
        }
    }

    @Override
    public void onBackPressed() {
        if (et_search.isFocused()){
            //just back
            super.onBackPressed();
        }
        else if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
            drawerLayout.closeDrawer(GravityCompat.END);
        }
        else{
            finish();
        }
    }

}
