package com.example.store;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

public class ActivitySplash extends AppCompatActivity {

    private static int Splash_time_out = 4000;

    Button btn_start;
    Animation from_bottom;
    Animation from_top;
    ImageView image;
    ImageView img_umbrella;

    MediaPlayer music;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        image = findViewById(R.id.img_splash);
        img_umbrella = findViewById(R.id.img_umb);

        music = MediaPlayer.create(this,R.raw.sound);

        from_bottom = AnimationUtils.loadAnimation(this, R.anim.from_bottom_to_top);
        from_bottom.setDuration(2000);

        from_top = AnimationUtils.loadAnimation(this, R.anim.from_top);
        from_top.setDuration(2000);

        image.setAnimation(from_top);
        img_umbrella.setAnimation(from_top);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(ActivitySplash.this, ActivityMain.class);
                startActivity(intent);
                finish();
            }
        },Splash_time_out);

    }

    @Override
    protected void onStart() {
        super.onStart();
        music.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        music.release();
    }
}
